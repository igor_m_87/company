#include <DeveloperTask.h>

DeveloperTask::DeveloperTask():
    Task(Task::DEVELOPER, Task::UNKNOWN_TASK)
{

}

DeveloperTask::DeveloperTask(Task::TaskGroup group, Task::TaskType type):
    Task(group,type)
{

}

DeveloperTask::DeveloperTask(const Task &task):Task(task)
{

}
