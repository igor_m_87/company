#include <WriterTask.h>

WriterTask::WriterTask():
    Task(Task::WRITER,Task::UNKNOWN_TASK)
{

}

WriterTask::WriterTask(Task::TaskGroup group, Task::TaskType type):
    Task(group,type)
{

}

WriterTask::WriterTask(const Task &task):Task(task)
{

}
