#include <AccountantTask.h>

AccountantTask::AccountantTask():
    Task(Task::ACCOUNTANT,Task::UNKNOWN_TASK)
{

}

AccountantTask::AccountantTask(Task::TaskGroup group, Task::TaskType type):
    Task(group,type)
{

}

AccountantTask::AccountantTask(const Task &task):Task(task)
{

}
