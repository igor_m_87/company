#include <Task.h>
#include <Core.h>
#include <Employee.h>

std::unordered_map<size_t,std::string> Task::TASK_NAMES = {
        {static_cast<size_t>(Task::UNKNOWN_TASK)   ,"Unknown task"},
        {static_cast<size_t>(Task::WRITE_PROGRAM)  , "Write a program"},
        {static_cast<size_t>(Task::DESIGN_PROGRAM) , "Design a program"},
        {static_cast<size_t>(Task::TRANSLATE_TEXT) , "Translate a text"},
        {static_cast<size_t>(Task::TEST_PROGRAM)   , "Test a program"},
        {static_cast<size_t>(Task::PLAN_TEST)      , "Plan testing"},
        {static_cast<size_t>(Task::PAY_SALARY)     , "Pay salary"},
        {static_cast<size_t>(Task::FORM_REPORT)    , "Form a report"},
        {static_cast<size_t>(Task::HAVE_VACATION)  , "Have a vacation"},
        {static_cast<size_t>(Task::CLEAN_WORKPLACE), "Clean a workpalce"}
    };

Task::Task():_group(UNKNOWN_GROUP),_type(UNKNOWN_TASK)
{

}

Task::Task(Task::TaskGroup group, Task::TaskType type):
    _group(group),_type(type)
{

}

Task::Task(const Task& task)
{
    this->_group = task._group;
    this->_type  = task._type;
}

Task& Task::operator=(const Task& task)
{
    if(this != &task)
    {
        this->_group = task._group;
        this->_type  = task._type;
    }
    return *this;
}

std::string Task::taskName(Task::TaskType type)
{
    return TASK_NAMES[type];
}

Task::TaskGroup Task::getEmployeeGroup(const Employee* employee)
{
    std::string position = employee->getPosition();

    if(position == Core::DEVELOPER)
        return Task::DEVELOPER;
    else if(position == Core::TESTER)
        return Task::TESTER;
    else if(position == Core::WRITER)
        return Task::WRITER;
    else if(position == Core::ACCOUNTANT)
        return Task::ACCOUNTANT;

    return Task::UNKNOWN_GROUP;
}
