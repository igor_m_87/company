#include <TesterTask.h>

TesterTask::TesterTask():
    Task(Task::TESTER,Task::UNKNOWN_TASK)
{

}

TesterTask::TesterTask(Task::TaskGroup group, Task::TaskType type):
    Task(group,type)
{

}

TesterTask::TesterTask(const Task &task):Task(task)
{

}
