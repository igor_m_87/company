#include <TaskFactory.h>
#include <DeveloperTask.h>
#include <TesterTask.h>
#include <WriterTask.h>
#include <AccountantTask.h>

Task* TaskFactory::createTask(Task::TaskGroup taskGroup, Task::TaskType taskType)
{
    switch(taskGroup)
    {
        case Task::TaskGroup::UNKNOWN_GROUP :
            return nullptr;

        case Task::TaskGroup::DEVELOPER :
            return new DeveloperTask(taskGroup, taskType);

        case Task::TaskGroup::TESTER :
            return new TesterTask(taskGroup, taskType);

        case Task::TaskGroup::WRITER :
            return new WriterTask(taskGroup, taskType);

        case Task::TaskGroup::ACCOUNTANT :
            return new AccountantTask(taskGroup, taskType);
    }

    return nullptr;
}
