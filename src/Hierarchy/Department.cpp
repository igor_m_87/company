#include <Department.h>

Department::Department(const std::string& name):_name(name)
{

}

Department::Department(const Department& department)
{
    this->_name = department._name;
    this->_employees = department._employees;
}

Department::~Department()
{
    for(auto empl : _employees)
        delete empl.second;
}

Department& Department::operator=(const Department& department)
{
    if(this != &department)
    {
        this->_name = department._name;
        this->_employees = department._employees;
    }

    return *this;
}

void Department::addEmployee(Employee* employee)
{
    if(!employee) return;

    _employees.insert(std::pair<std::string, Employee*>(employee->name(), employee));
}
