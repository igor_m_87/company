#include <Developer.h>
#include <Task.h>

Developer::Developer()
{
    initValidTasks();
}

Developer::Developer(const std::string& name):Employee(name)
{
    initValidTasks();
}

Developer::Developer(const Developer& developer):Employee(developer)
{

}

Developer::~Developer()
{

}

void Developer::initValidTasks()
{
    _validTasks.insert(Task::TaskType::HAVE_VACATION);
    _validTasks.insert(Task::TaskType::CLEAN_WORKPLACE);
    _validTasks.insert(Task::TaskType::WRITE_PROGRAM);
    _validTasks.insert(Task::TaskType::DESIGN_PROGRAM);
}
