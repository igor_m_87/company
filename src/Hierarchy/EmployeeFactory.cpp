#include <EmployeeFactory.h>
#include <Core.h>
#include <Director.h>
#include <Chief.h>
#include <Developer.h>
#include <Tester.h>
#include <Writer.h>
#include <Accountant.h>

#include <string>

Employee* EmployeeFactory::createEmployee(const std::string& position,
                                          const std::string& name)
{
    if(position == Core::CHIEF)
        return new Chief(name);
    else if(position == Core::DEVELOPER)
        return new Developer(name);
    else if(position == Core::TESTER)
        return new Tester(name);
    else if(position == Core::WRITER)
        return new Writer(name);
    else if(position == Core::ACCOUNTANT)
        return new Accountant(name);

    return nullptr;
}
