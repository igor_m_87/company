#include <Employee.h>
#include <iostream>

Employee::Employee(const std::string& name):_name(name)
{

}

Employee::Employee(const Employee& employee)
{
    this->_name = employee._name;
    this->_validTasks = employee._validTasks;
}

bool Employee::doTask(const Task *task) const
{
    Task::TaskType task_type = task->type();

    std::cout << "-------Report----------------"<<std::endl;
    std::cout << "From: " << _name << " (" << getPosition() << ")"<< std::endl;
    std::cout << "Task: " << task->taskName(task_type) << std::endl;

    auto it = _validTasks.find(task_type);

    if(it == _validTasks.end())
    {
        std::cout << "Result:" << NEGATIVE_REPORT << std::endl << std::endl;
        return false;
    }
    else
    {
        std::cout << "Result:" << POSITIVE_REPORT << std::endl << std::endl;
        return true;
    }
}
