#include <Writer.h>
#include <Task.h>

Writer::Writer()
{
    initValidTasks();
}

Writer::Writer(const std::string& name):Employee(name)
{
    initValidTasks();
}

Writer::Writer(const Writer& writer):Employee(writer)
{

}

Writer::~Writer()
{

}

void Writer::initValidTasks()
{
    _validTasks.insert(Task::TaskType::HAVE_VACATION);
    _validTasks.insert(Task::TaskType::CLEAN_WORKPLACE);
    _validTasks.insert(Task::TaskType::TRANSLATE_TEXT);
}
