#include <Accountant.h>
#include <Task.h>

#include <iostream>

Accountant::Accountant()
{
    initValidTasks();
}

Accountant::Accountant(const std::string& name):Employee(name)
{
    initValidTasks();
}

Accountant::Accountant(const Accountant &accountant):Employee(accountant)
{

}

Accountant::~Accountant()
{

}

void Accountant::initValidTasks()
{
    _validTasks.insert(Task::TaskType::HAVE_VACATION);
    _validTasks.insert(Task::TaskType::CLEAN_WORKPLACE);
    _validTasks.insert(Task::TaskType::PAY_SALARY);
    _validTasks.insert(Task::TaskType::FORM_REPORT);
}
