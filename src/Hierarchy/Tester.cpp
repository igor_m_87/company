#include <Tester.h>
#include <Task.h>

Tester::Tester()
{
    initValidTasks();
}

Tester::Tester(const std::string& name):Employee(name)
{
    initValidTasks();
}

Tester::Tester(const Tester& tester):Employee(tester)
{

}

Tester::~Tester()
{

}

void Tester::initValidTasks()
{
    _validTasks.insert(Task::TaskType::HAVE_VACATION);
    _validTasks.insert(Task::TaskType::CLEAN_WORKPLACE);
    _validTasks.insert(Task::TaskType::TEST_PROGRAM);
    _validTasks.insert(Task::TaskType::PLAN_TEST);
}
