#include <iostream>
#include <Core.h>

int main()
{
    std::string dirPath = Core::core().getConfigDir();

    if(!Core::core().loadStructure(dirPath))
    {
        std::cout << "error: " << Core::core().getLastError() << std::endl;
        return 0;
    }

    Core::core().run();

    return 0;
}
