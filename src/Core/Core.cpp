#include <Core.h>
#include <Director.h>
#include <Interface.h>
#include <Developer.h>
#include <Writer.h>
#include <Chief.h>
#include <Tester.h>
#include <Accountant.h>
#include <TaskFactory.h>
#include <EmployeeFactory.h>

#include <iostream>
#include <fstream>
#include <boost/filesystem.hpp>

Core& Core::core()
{
    static Core core;
    return core;
}

Core::Core():_last_error(""){}

Core::~Core()
{
    for(auto dep : _departments)
        delete dep.second;
}

bool Core::loadStructure(const std::string& directory)
{
    namespace fs = boost::filesystem;
    fs::path dir_path(directory);
    if(!fs::exists(dir_path) || !fs::is_directory(dir_path)){
        _last_error = directory + " is not an existing directory";
        return false;
    }

    fs::directory_iterator end_it;
    const std::string EXT = ".csv";
    for(fs::directory_iterator dir_it(dir_path);dir_it != end_it;++dir_it)
    {
        if(fs::is_regular_file(dir_it->path()) && dir_it->path().extension() == EXT)
        {
            if(!readCsv(dir_it->path().string()))
                std::cout << "File reading error: " << _last_error << dir_it->path().string() << std::endl;

        }
    }

    return true;
}

std::string Core::getConfigDir()
{
    std::cout << "Do you want use default configuration directory (" << DEFAULT_CONFIG_PATH << ")(Y/n)? ";
    std::string str = "";
    std::cin >> str;
    if(str == "y" || str =="Y" || str.empty())
    {
        return DEFAULT_CONFIG_PATH;
    }
    else if(str == "n" || str =="N")
    {
        std::cout << "Input configuration files directory . . . " << std::endl;
        std::cin >> str;
    }

    return str;
}

void Core::printStructure()
{
    system("clear");
    std::cout << "Director:" << Director::director().name() << std::endl;
    for(auto dep_pair: _departments)
    {
        Department* dep = dep_pair.second;
        std::cout << "Department: " << dep->name() << std::endl;
        for(auto employee_pair: dep->employees())
        {
            Employee* employee = employee_pair.second;
            std::cout << "\t" << employee->getPosition() << ":" << employee->name() << std::endl;
        }
    }
}

void Core::run()
{
    while(true)
    {
        Interface::Actions action = Interface::interface().showActions();

        if(action == Interface::Actions::BROADCAST_TASK)
        {
            setBroadcastTask();
        }
        else if (action == Interface::Actions::DEPARTMENT_TASK)
        {
            setDepartmentTask();
        }
        else if (action == Interface::Actions::EMPLOYEE_TASK)
        {
            setEmployeeTask();
        }
        else if(action == Interface::Actions::PRINT_STRUCTURE)
        {
            Core::core().printStructure();
            std::cin.get();
        }
        else if(action == Interface::Actions::EXIT) break;

        std::cin.get();
    }
}

void Core::setBroadcastTask()
{
    Task::TaskType taskType = Interface::interface().showAllTasks();
    if(taskType == Task::UNKNOWN_TASK)
        return;

    auto departments = Core::departments();
    for(auto dep_pair : departments)
    {
        Department* dep = dep_pair.second;
        if(!dep)
            continue;

        runDepartmentTask(dep, taskType);
    }
    std::cin.get();
}

void Core::setDepartmentTask()
{
    Department* dep = Interface::interface().showDepartments();
    if(dep)
    {
        Task::TaskType taskType = Interface::interface().showAllTasks();
        if(taskType == Task::UNKNOWN_TASK)
            return;

        runDepartmentTask(dep, taskType);
        std::cin.get();
    }
}

void Core::setEmployeeTask()
{
    Department* dep = Interface::interface().showDepartments();
    if(!dep)return;

    Employee* employee = Interface::interface().showEmployees(*dep);
    if(!employee) return;

    Task::TaskType taskType = Interface::interface().showTasks(employee->validTasks());
    runEmployeeTask(employee, taskType);
    std::cin.get();
}

void Core::runEmployeeTask(const Employee* employee, Task::TaskType taskType)
{
    if(!employee )return;

    Task::TaskGroup taskGroup = Task::getEmployeeGroup(employee);
    Task* task = TaskFactory::createTask(taskGroup, taskType);
    if(!task)return;

    if(task->type() != Task::UNKNOWN_TASK)
        employee->doTask(task);

    delete task;
}

void Core::runDepartmentTask(Department* dep, Task::TaskType taskType)
{
    if(!dep)return;

    auto employees = dep->employees();
    for(auto pair : employees)
    {
        Employee* employee = pair.second;
        if(!employee) continue;

        runEmployeeTask(employee, taskType);
    }
}

bool Core::readCsv(const std::string& file_path)
{
    std::ifstream ifs(file_path);
    if(!ifs)
    {
        _last_error = "Can't open the file: " + file_path + " doesn't exist";
        return false;
    }

    namespace fs = boost::filesystem;
    fs::path path(file_path);
    std::string file_name = path.filename().string();

    Department* dep = nullptr;
    if(file_name != DIRECTOR_FILE)
    {
        dep = new Department(path.stem().string());
        std::pair<std::string,Department*> dep_pair (dep->name(),dep);
        _departments.emplace(dep_pair);
    }

    std::string str = "";
    while(ifs >> str)
    {
        if(str == CSV_HEADER)
            continue;

        int pos = str.find(DELIMITER);
        if(pos == std::string::npos)
            std::cout << "File structure error found: " << file_path << std::endl;
        std::string name = str.substr(0,pos);
        std::string position = str.substr(pos+1);

        if(position == DIRECTOR)
        {
            Director::director().setName(name);
        }
        else
        {
            Employee* employee = EmployeeFactory::createEmployee(position,name);
            if(!employee)
                std::cout << "Anknown position found in " + file_path << std::endl;
            else
                dep->addEmployee(employee);
        }
    }

    return true;
}
