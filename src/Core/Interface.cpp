#include <Interface.h>
#include <Core.h>
#include <Department.h>

#include <iostream>

Interface& Interface::interface()
{
    static Interface interf;
    return interf;
}

Interface::Actions Interface::showActions()
{
    system("clear");
    std::cout << "********* Company management programm *********" << std::endl;
    std::cout << "        1 - Set task for all employes" << std::endl;
    std::cout << "        2 - Set task for department" << std::endl;
    std::cout << "        3 - Set task for employee" << std::endl;
    std::cout << "        4 - Print company structure" << std::endl;
    std::cout << "        5 - Stop the programm" << std::endl;

    size_t res = getMenuItem();
    return static_cast<Actions>(res);
}

Department* Interface::showDepartments()
{
    system("clear");
    std::cout << "********* Departments list *********" << std::endl;

    size_t cnt = 0;
    Core::Departments departments = Core::core().departments();
    for(auto dep_pair : departments)
        std::cout << "        " << ++cnt << " - " << dep_pair.first << std::endl;
    std::cout << "        " << ++cnt << " - Go back to the main menu" << std::endl;

    size_t res = getMenuItem();

    if(res > departments.size())
        return nullptr;

    auto dep_it = departments.begin();
    for(int i = 0;i < res - 1;i++)
        dep_it++;

    Department* dep = (*dep_it).second;

    return dep;
}

Task::TaskType Interface::showAllTasks()
{
    Task::Tasks taskMap = Task::getTaskList();

    std::set<Task::TaskType> tasks;
    for(auto pair : taskMap)
    {
        if(pair.first == Task::UNKNOWN_TASK)
            continue;

        tasks.insert(static_cast<Task::TaskType>(pair.first));
    }

    return showTasks(tasks);
}

Task::TaskType Interface::showTasks(const std::set<Task::TaskType>& taskList)
{
    system("clear");
    std::cout << "********* Task List *********" << std::endl;

    size_t cnt = 0;
    for(auto it : taskList)
        std::cout << "        " << ++cnt << " - " << Task::taskName(it) << std::endl;
    std::cout << "        " << ++cnt << " - Go back to the main menu" << std::endl;

    size_t res = getMenuItem();
    if(res > taskList.size())
        return Task::TaskType::UNKNOWN_TASK;

    auto it = taskList.begin();
    for(int i = 0; i < res - 1;i++)
        it++;

    return *it;
}

Employee* Interface::showEmployees(const Department& department)
{
    system("clear");
    std::cout << "********* Employee List *********" << std::endl;

    size_t cnt = 0;
    Department::Employees employees = department.employees();
    for(auto employee : employees)
        std::cout << "        " << ++cnt << " - " << employee.first << "(" << employee.second->getPosition() << ")" << std::endl;
    std::cout << "        " << ++cnt << " - Go back to the main menu" << std::endl;

    size_t res = getMenuItem();
    if(res > employees.size())
        return nullptr;

    auto emp_it = employees.begin();
    for(int i = 0; i < res - 1;i++)
        emp_it++;

    Employee* employee = (*emp_it).second;

    return employee;
}

size_t Interface::getMenuItem()
{
    size_t res = 0;
    std::string str;
    std::cin >> str;
    try
    {
        res = std::stoi(str);
    }
    catch(std::invalid_argument)
    {
        res = 0;
    }

    return res;
}
