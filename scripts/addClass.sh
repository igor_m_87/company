#!/bin/bash

# First two checkings work strange.
# You must fix it when you have enough time.
if ! [ -n $1 ]; then
    echo "invalid sub directory"
    exit 0
fi


if ! [ -n $2 ]; then
    echo "invalid class name"
    exit 0
fi

SUB_DIRECTORY=$1
CLASS_NAME=$2


if ! [ -d ./../include/$SUB_DIRECTORY ]; then
    mkdir ./../include/$SUB_DIRECTORY
fi

if ! [ -d ./../src/$SUB_DIRECTORY ]; then
    mkdir ./../src/$SUB_DIRECTORY
fi

touch ./../include/$SUB_DIRECTORY/$CLASS_NAME.h
touch ./../src/$SUB_DIRECTORY/$CLASS_NAME.cpp