#pragma once

#include <set>
#include <string>
#include <stddef.h>
#include <Task.h>

class Department;
class Employee;

class Interface
{
public:
    enum Actions {UNDEFINED = 0, BROADCAST_TASK,DEPARTMENT_TASK, EMPLOYEE_TASK, PRINT_STRUCTURE, EXIT};

    static Interface& interface();

    Actions showActions();
    Department* showDepartments();
    Task::TaskType showAllTasks();
    Task::TaskType showTasks(const std::set<Task::TaskType> &taskList);
    Employee* showEmployees(const Department& department);
private:
    Interface() = default;
    Interface(const Interface& interf) = delete;
    ~Interface() = default;
    Interface& operator=(const Interface& interf) = delete;
    size_t getMenuItem();
};
