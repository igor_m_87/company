#pragma once

#include <Department.h>

#include <unordered_map>

class Director;

class Core
{
public:
    using Departments = std::unordered_map<std::string,Department*>;
    static Core& core();

    bool loadStructure(const std::string& directory);
    std::string getConfigDir();
    inline std::string getLastError() const {return _last_error;}
    void printStructure();
    const Departments& departments() const {return _departments;}
    void run();

// Positions
    constexpr static const char* DIRECTOR = "Director";
    constexpr static const char* CHIEF = "Chief";
    constexpr static const char* DEVELOPER = "Developer";
    constexpr static const char* WRITER = "Writer";
    constexpr static const char* ACCOUNTANT = "Accountant";
    constexpr static const char* TESTER = "Tester";

private:
    Core();
    ~Core();
    Core(const Core& core) = delete;
    Core& operator=(const Core& core) = delete;

    void setBroadcastTask();
    void setDepartmentTask();
    void setEmployeeTask();
    void runEmployeeTask(const Employee* employee, Task::TaskType taskType);
    void runDepartmentTask(Department* dep, Task::TaskType taskType);
    bool readCsv(const std::string& file_path);

    Departments _departments;

    constexpr static const char* DIRECTOR_FILE = "Director.csv";
    constexpr static const char* CSV_HEADER = "Name;Position";
    constexpr static const char  DELIMITER = ';';
    constexpr static const char* DEFAULT_CONFIG_PATH = "./../test_data";

    std::string _last_error;
};

