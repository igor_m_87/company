#pragma once

#include <Task.h>

class WriterTask: public Task
{
public:
    WriterTask();
    WriterTask(TaskGroup group, TaskType type);
    WriterTask(const Task& task);
    ~WriterTask()=default;

    WriterTask& operator =(const WriterTask& task) = default;
};
