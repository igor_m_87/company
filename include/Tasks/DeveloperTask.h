#pragma once

#include <Task.h>

class DeveloperTask: public Task
{
public:
    DeveloperTask();
    DeveloperTask(TaskGroup group, TaskType type);
    DeveloperTask(const Task& task);
    ~DeveloperTask()=default;

    DeveloperTask& operator =(const DeveloperTask& task) = default;
};
