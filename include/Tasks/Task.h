#pragma once

#include <string>
#include <unordered_map>

class Employee;

class Task
{
public:
    using Tasks = std::unordered_map<size_t,std::string>;

    enum TaskGroup {UNKNOWN_GROUP=0,COMMON,DEVELOPER,TESTER,WRITER,ACCOUNTANT};
    enum TaskType
    {
        UNKNOWN_TASK = 0,
        WRITE_PROGRAM,DESIGN_PROGRAM,  // developer tasks
        TRANSLATE_TEXT,                // writer tasks
        TEST_PROGRAM, PLAN_TEST,       // tester tasks
        PAY_SALARY, FORM_REPORT,       // accountant tasks
        HAVE_VACATION, CLEAN_WORKPLACE // common tasks
    };

    Task();
    Task(TaskGroup group, TaskType type);
    Task(const Task& task);
    virtual ~Task()=default;

    Task& operator=(const Task& task);
    inline TaskGroup group() const {return _group;}
    inline TaskType type() const {return _type;}
    static std::string taskName(TaskType type);
    static Task::TaskGroup getEmployeeGroup(const Employee* employee);
    static inline const Tasks& getTaskList() {return TASK_NAMES;}
protected:
    TaskType _type;
    TaskGroup _group;
    static Tasks TASK_NAMES;
};
