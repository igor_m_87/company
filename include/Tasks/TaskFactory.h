#pragma once

#include <Task.h>

class TaskFactory
{
public:
    static Task* createTask(Task::TaskGroup taskGroup, Task::TaskType taskType);
private:
    TaskFactory()=delete;
    ~TaskFactory()=delete;
    TaskFactory(const TaskFactory&) = delete;

    TaskFactory& operator=(const TaskFactory&) = delete;
};
