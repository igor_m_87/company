#pragma once

#include <Task.h>

class AccountantTask: public Task
{
public:
    AccountantTask();
    AccountantTask(TaskGroup group, TaskType type);
    AccountantTask(const Task& task);
    ~AccountantTask()=default;

    AccountantTask& operator =(const AccountantTask& task) = default;
};
