#pragma once

#include <Task.h>

class TesterTask: public Task
{
public:
    TesterTask();
    TesterTask(TaskGroup group, TaskType type);
    TesterTask(const Task& task);
    ~TesterTask()=default;

    TesterTask& operator =(const TesterTask& task) = default;
};
