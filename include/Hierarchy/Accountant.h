#pragma once

#include <Employee.h>
#include <Core.h>

#include <string>

class Accountant : public Employee
{
public:
    Accountant();
    Accountant(const std::string& name);
    Accountant(const Accountant& accountant);
    ~Accountant();

    Accountant& operator=(const Accountant& accountant) = default;
    virtual std::string getPosition() const override { return Core::core().ACCOUNTANT ;}
private:
    virtual void initValidTasks() override;
};
