#pragma once

#include <Employee.h>

#include <unordered_map>
#include <string>

class Department
{
public:
    using Employees = std::unordered_map<std::string,Employee*>;

    Department() = default;
    Department(const std::string& name);
    Department(const Department& department);
    ~Department();

    Department& operator=(const Department& department);

    inline std::string name() const {return _name;}
    inline std::string setName(const std::string& name){_name = name;}

    void addEmployee(Employee* employee);
    const Employees& employees() const {return _employees;}

private:
    Employees _employees;
    std::string _name;
};
