#pragma once

#include <string>

class Employee;

class EmployeeFactory
{
public:
    static Employee* createEmployee(const std::string& position,
                                    const std::string& name);
private:
    EmployeeFactory()=delete;
    EmployeeFactory(const EmployeeFactory&)=delete;
    ~EmployeeFactory()=delete;
    EmployeeFactory& operator=(const EmployeeFactory&)=delete;
};
