#pragma once

#include <Employee.h>
#include <Core.h>

#include <string>

class Developer : public Employee
{
public:
    Developer();
    Developer(const std::string& name);
    Developer(const Developer& developer);
    ~Developer();

    Developer& operator=(const Developer& developer) = default;
    virtual std::string getPosition() const override { return Core::core().DEVELOPER ;}
private:
    virtual void initValidTasks() override;
};
