#pragma once

#include <Employee.h>
#include <Core.h>

#include <string>

class Writer : public Employee
{
public:
    Writer();
    Writer(const std::string& name);
    Writer(const Writer& writer);
    ~Writer();

    Writer& operator=(const Writer& writer) = default;
    virtual std::string getPosition() const override { return Core::core().WRITER;}
private:
    virtual void initValidTasks() override;
};
