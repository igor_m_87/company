#pragma once

#include <Task.h>

#include <string>
#include <set>

class Employee
{
public:
    Employee()=default;
    Employee(const std::string& name);
    Employee(const Employee& employee);
    virtual ~Employee() = default;

    Employee& operator=(const Employee& employee);

    inline std::string name() const {return _name;}
    inline void setName(const std::string& name) {_name = name;}

    virtual std::string getPosition() const = 0;
    bool doTask(const Task* task) const;

    inline const std::set<Task::TaskType>& validTasks() {return _validTasks;}
protected:
    virtual void initValidTasks() = 0;

    std::string _name;
    std::set<Task::TaskType> _validTasks;

    constexpr static const char* POSITIVE_REPORT = "The task has been done.";
    constexpr static const char* NEGATIVE_REPORT = "The employee can't do the task.";
};
