#pragma once

#include <Employee.h>
#include <Core.h>

#include <string>

class Chief : public Employee
{
public:
    Chief() = default;
    Chief(const std::string& name): Employee(name){}
    Chief(const Chief& chief) : Employee(chief){}
    ~Chief();

    Chief& operator=(const Chief& chief) = default;
    virtual std::string getPosition() const override { return Core::core().CHIEF;}
private:
    virtual void initValidTasks() override{}
};
