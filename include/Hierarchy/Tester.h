#pragma once

#include <Employee.h>
#include <Core.h>

#include <string>

class Tester : public Employee
{
public:
    Tester();
    Tester(const std::string& name);
    Tester(const Tester& tester);
    ~Tester();

    Tester& operator=(const Tester& tester) = default;
    virtual std::string getPosition() const override { return Core::core().TESTER;}
private:
    virtual void initValidTasks() override;
};
