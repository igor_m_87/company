#pragma once

#include <Employee.h>
#include <Core.h>

#include <string>

class Director : public Employee
{
public:
    static Director& director();

private:
    Director(){}
    Director(const std::string& name):Employee(name){}
    Director(const Director& director) = delete;
    ~Director();

    Director& operator=(const Director& director) = delete;
    virtual std::string getPosition() const override { return Core::core().DIRECTOR;}
    virtual void initValidTasks() override{}
};
